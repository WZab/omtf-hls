#include<stdlib.h>
#include<stdio.h>
#include "golden_proc_pkg.h"
#include "omtf_functions.h"
#define MAX_BXES (100)
T_GP_HITS_ARRAY hits_in[MAX_BXES];
T_GP_OUTPUT res;
int main(int argc, char * argv[]) 
{
  ap_uint<1> load = 0;
  //Clear the table
  for(int i=0;i<MAX_BXES;i++)
	  for(int j=0;j<GP_MAX_LAYERS;j++)
		  for(int k=0;k<GP_MAX_INS_IN_LAYER;k++)
			  hits_in[i][j][k]=GP_EMPTY_HIT;
  //Open the hit definition files
  FILE * fi=fopen("/tmp/hits.ini","r");
  if(!fi){
	  perror("Can't open file!\n");
	  exit(1);
  }
  while(1) {
    //We iterate through BXes
    int layer;
    int entry;
    int phi;
    int hbx;
    int ne = fscanf(fi,"%d %d %d %d\n",&layer,&entry,&phi,&hbx);
    if(ne < 4) break;
    hits_in[hbx][layer][entry].phi = phi;
    hits_in[hbx][layer][entry].active = 1;
    // Tak byla generowana Eta w symulcjach
    // data_out(lay)(entry).eta      <= std_logic_vector(to_unsigned(bx_in+lay+entry mod 512,9));
    hits_in[hbx][layer][entry].eta = (hbx+layer+entry) % 512;

    //printf("bx:%d, layer=%d,entry=%d,phi=%d\n",hbx,layer,entry,phi);
  }
  fclose(fi);
  //Now we play the hits:
  for(int bx=0;bx<MAX_BXES;bx++) {
	  printf("Start BX:%d\n",bx);
	  for(int nstep=0;nstep<4;nstep++) {
		  ap_uint<1> load = (nstep==3) ? 1 : 0;
		  res=golden_proc_hls(bx,load,hits_in[bx]);
 		if (res.found) {
				printf("The best pattern id:%d\n", (int) res.best_pat);
				printf("  Number of layers:%d\n", (int) res.fired_cnt);
				printf("  Fired layers:");
				for(int i=0;i<GP_MAX_LAYERS;i++)
					printf("%d",(int) res.fired_vec[i]);
				printf("\n");
				printf("  Sum of weight:%d\n", (int) res.weight_sum);
				printf("  Ref_eta:%d\n", (int) res.ref_eta);
				printf("  Ref_phi:%d\n", (int) res.ref_phi);
				printf("  Corr_ref_phi:%d\n", (int) res.corr_ref_phi);
				printf("  Ref_layer:%d\n\n", (int) res.ref_layer);
			}

	  }
  }
  //
}
