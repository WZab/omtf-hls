-------------------------------------------------------------------------------
-- Title      : Connection area builder
-- Project    : CMS OTF
-------------------------------------------------------------------------------
-- File       : conn_area_builder.vhd
-- Author     : WZab
-- Company    : ISE PW, IFD UW
-- Created    : 2014-02-11
-- Last update: 2017-11-22
-- Platform   : 
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description: "Connection Area" builder
--              Currently introduces latency of 1 clkp
-------------------------------------------------------------------------------
-- Copyright (c) 2014 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2014-02-11  1.0      WZab    Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
library work;
use work.omtf_constants_pkg.all;
use work.golden_proc_pkg.all;
use work.conn_area_pkg.all;

entity conn_area_builder is
  port (
    hits_in   : in  T_GP_HITS_ARRAY;
    conar_out : out T_GP_CONAR_OUTS;
    conar_nr  : in  T_GP_CONAR_NR;
    refHit    : in  T_GP_HIT;
    clk       : in  std_logic;
    rst_n     : in  std_logic);

end entity conn_area_builder;

architecture beh1 of conn_area_builder is

  signal s_conar_out : T_GP_CONAR_OUTS := (others => (others => GP_EMPTY_HIT_INTERNAL));


  --function conn_area_builder_init
  --  return T_GP_ALL_CONARS_DEF is
  --  file cadefs   : text;
  --  variable line_in : line;
  --  variable first,len    : integer;
  --  variable res : T_GP_ALL_CONARS_DEF;
  --begin  -- function ref_hit_dec_init
  --  file_open(cadefs,"conf/conn_area_build.ini", read_mode);
  --  for i in 0 to GP_N_OF_CONARS-1 loop
  --    --if endfile(cadefs) then
  --    --  report "Connection Area Definition file was too short" severity error;
  --    --end if;
  --    readline(cadefs, line_in);
  --    for j in 0 to GP_MAX_LAYERS-1 loop
  --      read(line_in, first);
  --      res(i)(j).first := first;
  --      read(line_in, len);
  --      res(i)(j).len := len;
  --      report "CA def:" & integer'image(first) & "," & integer'image(len) severity note;
  --    end loop;  -- j
  --  end loop;  -- i
  --  file_close(cadefs);
  --  return res;
  --end function conn_area_builder_init;

  constant CONARS_DEF : T_GP_ALL_CONARS_DEF := conn_area_builder_init;

begin  -- beh1

  p1 : process (clk)
    variable hit_internal : T_GP_HIT_INTERNAL := GP_EMPTY_HIT_INTERNAL;
  begin  -- process p1
    if clk'event and clk = '1' then     -- rising clock edge
      --if rst_n = '0' then               -- asynchronous reset (active low)
      --  s_conar_out <= (others => (others => GP_EMPTY_HIT_INTERNAL));
      --else
      s_conar_out <= (others => (others => GP_EMPTY_HIT_INTERNAL));
      -- Iterate through layers
      for layer in 0 to GP_MAX_LAYERS-1 loop
        -- Iterate through inputs
        for nin in 0 to GP_MAX_INS_IN_LAYER loop
          if (nin < CONARS_DEF(conar_nr)(layer).len) and (nin < 6) then
            -- Please note, that we have added subtraction of the retHitPhi
            -- already here!
            --assert (1 = 0) report "nin=" & integer'image(nin) & " len=" & integer'image(CONARS_DEF(conar_nr)(layer).len) severity note;
            -- Copy full hit info
            hit_internal := hit2internal(hits_in(layer)(nin+CONARS_DEF(conar_nr)(layer).first));
            -- Calculate the dist_phi depending on
            if CONARS_DEF(conar_nr)(layer).bending then
              hit_internal.dist_phi := to_signed(to_integer(hit_internal.phi), GP_PHI_INTERNAL_WIDTH);
            else
              hit_internal.dist_phi := to_signed(to_integer(hit_internal.phi)-to_integer(refHit.phi), GP_PHI_INTERNAL_WIDTH);
            end if;
            s_conar_out(layer)(nin) <= hit_internal;
          end if;
        end loop;  -- nin
      end loop;  -- layer
    --end if;
    end if;
  end process p1;
  conar_out <= s_conar_out;

end beh1;
