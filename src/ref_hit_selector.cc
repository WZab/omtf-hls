/*-------------------------------------------------------------------------------
-- Title      : Reference hit selector
-- Project    : CMS OTF
-------------------------------------------------------------------------------
-- File       : ref_hit_selector.vhd
-- Author     : WZab
-- Company    : ISE PW, IFD UW
-- Created    : 2014-02-11
-- Last update: 2017-11-22
-- Platform   : 
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description: This block fills the reference hits vector and
--              copies the reference hit data.
--              It introduces the latency of 1 clock!
-------------------------------------------------------------------------------
-- Copyright (c) 2014 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2014-02-11  1.0      WZab    Created
-------------------------------------------------------------------------------
*/

//Jakoś muszę załatwić sprawę podwójnego wyjścia

//Przeglądamy opisy hitów referencyjnych
//  1. Sprawdzamy, czy na wejściu podanym w specyfikacji hitu ref. mamy hit o parametrach 
//     pasujących. Jeśli tak, to zaznaczamy hit jako trafiony i wypełniamy tabele
//     
#include "golden_proc_pkg.h"
void ref_hit_selector(const T_GP_REF_HIT_DEFS &ref_hit_conf,
  const T_GP_HITS_ARRAY &hits_in,
  ap_uint<1> ref_hits_bits[GP_N_OF_REF_HITS],
  T_GP_REF_HIT_DATA &ref_hits_data)
{
#pragma HLS ARRAY_PARTITION variable=hits_in complete dim=0
#pragma HLS DATA_PACK variable=hits_in
#pragma HLS ARRAY_PARTITION variable=ref_hits_bits complete
#pragma HLS DATA_PACK variable=ref_hits_bits
#pragma HLS ARRAY_PARTITION variable=ref_hits_data complete
#pragma HLS DATA_PACK variable=ref_hits_data
#pragma HLS UNROLL
//#pragma HLS INLINE
  int i;
  T_GP_REF_HIT_DEF conf;
  T_GP_HIT hit;
  for (i=0; i<GP_N_OF_REF_HITS;i++) {
#pragma HLS UNROLL
    // Initially mark the hit as empty
    ref_hits_bits[i]=0;
    ref_hits_data[i]=GP_EMPTY_HIT;
    // Check if the hit is active
    conf = ref_hit_conf[i];
    hit = hits_in[conf.layer][conf.entry];
    if (hit.active) {
      //printf("hit - layer=%d, entry=%d, phi=%d\n",conf.layer,conf.entry,hit.phi);
      if((hit.phi >= conf.min_phi) &&
      (hit.phi <= conf.max_phi)) {
    	 //printf("Found hit nr %d\n",i);
         ref_hits_bits[i] = 1;
         ref_hits_data[i] = hit;
      }
    }
  }
  return;
}

