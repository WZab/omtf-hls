-------------------------------------------------------------------------------
-- Title      : Reference hit selector
-- Project    : CMS OTF
-------------------------------------------------------------------------------
-- File       : ref_hit_selector.vhd
-- Author     : WZab
-- Company    : ISE PW, IFD UW
-- Created    : 2014-02-11
-- Last update: 2017-11-22
-- Platform   : 
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description: This block fills the reference hits vector and
--              copies the reference hit data.
--              It introduces the latency of 1 clock!
-------------------------------------------------------------------------------
-- Copyright (c) 2014 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2014-02-11  1.0      WZab    Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
library work;
use work.omtf_constants_pkg.all;
use work.golden_proc_pkg.all;

entity ref_hit_selector is
  generic (
    ref_hit_conf : T_GP_REF_HIT_DEFS);
  port (
    hits_in       : in  T_GP_HITS_ARRAY;
    ref_hits_bits : out std_logic_vector(GP_N_OF_REF_HITS-1 downto 0);
    ref_hits_data : out T_GP_REF_HIT_DATA;
    -- System interface
    load          : in  std_logic;
    clk           : in  std_logic;
    rst_n         : in  std_logic);

end entity ref_hit_selector;

architecture beh1 of ref_hit_selector is

begin
  process (clk) is
    variable hit  : T_GP_HIT;
    variable conf : T_GP_REF_HIT_DEF;
  begin  -- process
    if clk'event and clk = '1' then     -- rising clock edge
      --if rst_n = '0' then               -- asynchronous reset (active low)

      --else
      if load = '1' then
        for i in 0 to GP_N_OF_REF_HITS-1 loop
          -- Initially mark the hit as empty
          ref_hits_bits(i) <= '0';
          ref_hits_data(i) <= GP_EMPTY_HIT;
          -- Check if the hit is active
          conf             := ref_hit_conf(i);
          hit              := hits_in(conf.layer)(conf.entry);
          if (hit.active = '1') and
            (hit.phi >= conf.min_phi) and
            (hit.phi         <= conf.max_phi) then
            ref_hits_bits(i) <= '1';
            ref_hits_data(i) <= hit;
          end if;
        end loop;  -- i
      end if;
    --end if;
    end if;
  end process;

end beh1;
