/*-------------------------------------------------------------------------------
-- Title      : golden_proc package
-- Project    : CMS OTF
-------------------------------------------------------------------------------
-- File       : golden_proc_pkg.vhd
-- Author     : WZab
-- Company    : ISE PW, IFD UW
-- Created    : 2014-01-08
-- Last update: 2018-02-22
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Package defining constants for the golden pattern processor
-------------------------------------------------------------------------------
-- Copyright (c) 2014 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2014-01-08  1.0      WZab    Created
-------------------------------------------------------------------------------
*/
#ifndef __H_GOLDEN_PROC_PKG__
#define __H_GOLDEN_PROC_PKG__
#include <ap_fixed.h>
#include "../include/omtf_constants.h"
#include <assert.h>
// Pipeline delay when processing in a single layer
#define GP_DEL_IN_SNGL_LAYER (2)

//Distribution of bits in the LUT address
#define GP_REF_LAYER_BITS (3)
#define GP_SELECTED_PHI_BITS (0)
#define GP_SELECTED_DIST_PHI_BITS (7)

#define SPY_MEM_LOG2_LEN (10)
#define SPY_MEM_LEN (1<<SPY_MEM_LOG2_LEN)

#define GP_BEST_PAT_WIDTH (8)
typedef int T_GP_BX_NUM ;
#define GP_BX_NUM_INIT (0)

// subtype T_GP_CONAR_NR is integer range 0 to GP_N_OF_CONARS-1;
typedef ap_uint<3> T_GP_CONAR_NR;
// subtype T_GP_LAYER_NR is integer range 0 to GP_MAX_LAYERS-1;
typedef ap_uint<5> T_GP_LAYER_NR;
// subtype T_GP_ENTRY_NR is integer range 0 to GP_MAX_INS_IN_LAYER-1;
typedef ap_uint<4> T_GP_ENTRY_NR;
// subtype T_GP_REF_LAYER is integer range 0 to GP_MAX_REF_LAYERS-1;
typedef ap_uint<3> T_GP_REF_LAYER;
//  -- Layer, for which we calculate correction of ref_phi
#define GP_BASE_LAYER (2)
#define GP_PHI_WIDTH (11)
#define GP_ETA_WIDTH (9)
// Now I try to represent phi internally as integers, hoping that
// the synthesis tools will optimize calculations...
// If not, then we will introduce internal dist phi as a signed value
// two bits wider than the external phi
#define GP_PHI_INTERNAL_WIDTH (GP_PHI_WIDTH+2)
// The Phi values are coded as SIGNED (as we perform all calculations
// modulo 2**GP_PHI_WIDTH, it is not a problem

//  subtype T_GP_HIT_PHI is signed(GP_PHI_WIDTH-1 downto 0);
typedef ap_int<GP_PHI_WIDTH> T_GP_HIT_PHI;

// subtype T_GP_HIT_ETA is std_logic_vector(GP_ETA_WIDTH-1 downto 0);
typedef ap_int<GP_ETA_WIDTH> T_GP_HIT_ETA;

// The internal representation is longer by 2 bits to allow overflow detection
//subtype T_GP_HIT_DIST_PHI_INTERNAL is signed(GP_PHI_INTERNAL_WIDTH-1 downto 0);
typedef ap_int<GP_PHI_INTERNAL_WIDTH> T_GP_HIT_DIST_PHI_INTERNAL; 

//subtype T_GP_HIT_DIST_PHI_SORTER is signed(GP_SELECTED_DIST_PHI_BITS-1 downto 0);
typedef ap_int<GP_PHI_WIDTH> T_GP_HIT_DIST_PHI_SORTER;

//  --subtype T_GP_HIT_DIST_PHI is signed(GP_PHI_WIDTH-1 downto 0);
#define GP_HIT_DIST_PHI_MIN (-(1<<(GP_PHI_WIDTH-1)))
#define GP_HIT_DIST_PHI_MAX ((1<<(GP_PHI_WIDTH-1))-1)

// The type below holds the whole information associated with the type
// Currently this is only the phi (so in fact there is no need to use
// the record type), but we expect, that the amount of information
// may grow up...

typedef struct {
    ap_uint<1> active;
    T_GP_HIT_PHI phi;
    T_GP_HIT_ETA eta;
  } T_GP_HIT; 

//  -- We need the empty hit definition

const T_GP_HIT GP_EMPTY_HIT = {
    .active = 0,
    .phi = 0,
    .eta = 0,
    };

// The type below holds the internal representation of the hit
//  It is chosen so, that we can easily detect overflows.
typedef struct {
    ap_uint<1> active;
    T_GP_HIT_PHI phi;
    T_GP_HIT_DIST_PHI_INTERNAL dist_phi;
  } T_GP_HIT_INTERNAL;

// We need the empty hit definition
const T_GP_HIT_INTERNAL GP_EMPTY_HIT_INTERNAL = {
    .active   = 0,
    .phi      = 0,
    .dist_phi = 0,
    };

// Another definition is needed in the sorter
typedef struct {
    ap_uint<1> active;
    T_GP_HIT_DIST_PHI_SORTER adist_phi;
    ap_uint<1> sdist_phi;
  } T_GP_HIT_SORTER;

// We need the empty hit definition
const T_GP_HIT_SORTER GP_EMPTY_HIT_SORTER = {
    .active    = 0,
    .adist_phi = 0,
    .sdist_phi = 0
    };


//  -----------------------------------------------------------------------------
//  -- Definition of the type describing the construction of the Reference Hit
//  -- vector
//  -----------------------------------------------------------------------------
//-- Single reference hit definition
typedef struct {
    int layer; // integer range 0 to GP_MAX_LAYERS-1;
    int entry; // integer range 0 to GP_MAX_INS_IN_LAYER-1;
    int ref_layer; // integer range 0 to GP_MAX_REF_LAYERS-1;
    int conar_nr; // integer range 0 to GP_N_OF_CONARS-1;
    int min_phi;  // integer range -(2**(GP_PHI_WIDTH-1)) to (2**(GP_PHI_WIDTH-1))-1;
    int max_phi; // integer range -(2**(GP_PHI_WIDTH-1)) to (2**(GP_PHI_WIDTH-1))-1;
   } T_GP_REF_HIT_DEF;

// All reference hit definitions
typedef T_GP_REF_HIT_DEF T_GP_REF_HIT_DEFS[GP_N_OF_REF_HITS];

//All reference hits data
typedef  T_GP_HIT T_GP_REF_HIT_DATA[GP_N_OF_REF_HITS];

//  -----------------------------------------------------------------------------
//  -- Definition of the type needed to describe the distPhi sorter
//  -----------------------------------------------------------------------------

#define  GP_N_OF_DIST_PHI (6)
typedef T_GP_HIT_SORTER  T_GP_DIST_PHIS[GP_N_OF_DIST_PHI];

//  -----------------------------------------------------------------------------
//  -- Definition of data type used to define the connection area builder
//  -----------------------------------------------------------------------------

typedef T_GP_HIT T_GP_LAYER[GP_MAX_INS_IN_LAYER];
// type T_GP_HITS_ARRAY is array (0 to GP_MAX_LAYERS-1) of T_GP_LAYER;
typedef T_GP_LAYER T_GP_HITS_ARRAY[GP_MAX_LAYERS];
// type T_GP_CONAR_LAYER is array (0 to GP_MAX_CONAR_OUTS-1) of T_GP_HIT_INTERNAL;
typedef T_GP_HIT_INTERNAL T_GP_CONAR_LAYER[GP_MAX_CONAR_OUTS];
// type T_GP_CONAR_OUTS is array (0 to GP_MAX_LAYERS-1) of T_GP_CONAR_LAYER;
typedef  T_GP_CONAR_LAYER T_GP_CONAR_OUTS[GP_MAX_LAYERS];
// type T_GP_REF_HIT_INS is array (0 to 79) of T_GP_HIT;
typedef T_GP_HIT T_GP_REF_HIT_INS[80];
typedef struct {
  int first;   //: integer range 0 to GP_MAX_INS_IN_LAYER-1;
  int len;     //: integer range 1 to GP_MAX_INS_IN_LAYER;
  ap_uint<1>  bending; // : boolean;
  //used  : boolean;
} T_GP_CONAR_LAYER_DEF;

// type T_GP_CONAR_DEF is array (0 to GP_MAX_LAYERS-1) of T_GP_CONAR_LAYER_DEF;
typedef T_GP_CONAR_LAYER_DEF T_GP_CONAR_DEF[GP_MAX_LAYERS];
// type T_GP_ALL_CONARS_DEF is array (0 to GP_N_OF_CONARS-1) of T_GP_CONAR_DEF;
typedef T_GP_CONAR_DEF T_GP_ALL_CONARS_DEF[GP_N_OF_CONARS];
//  -----------------------------------------------------------------------------
//  -- Definitions needed to describe the Golden Pattern Processor
//  -----------------------------------------------------------------------------

//  --subtype T_GP_PDF_VAL is unsigned(17 downto 0);

// We check, that the length of the address is 10
//static_assert(GP_REF_LAYER_BITS + GP_SELECTED_PHI_BITS + GP_SELECTED_DIST_PHI_BITS == 10,"Length of the address must be equal to 10");
//  Definition of the data type used to configure a single pattern single layer unit
typedef ap_uint<36> T_PDF_ROM[1024];
//  --type T_PDF_ROM_AS_INT is array (0 to 1023) of integer;

//  --type T_GP_PDF_LUT is array (0 to 1023) of T_GP_PDF_VAL;
typedef ap_int<10> T_GP_MEAN_DIST_PHI[GP_MAX_REF_LAYERS];
typedef ap_uint<6> T_GP_SEL_DIST_PHI_SHIFT[GP_MAX_REF_LAYERS];// of integer range 0 to 32;
typedef ap_uint<6> T_GP_SEL_DIST_PHI_MSB_SHIFT[GP_MAX_REF_LAYERS];// of integer range 0 to 32;

//  -- Definition of the structure describing the configuration of a single
//  -- layer, single pattern unit

typedef struct {
  int nOfPhis; // integer range 0 to GP_MAX_CONAR_OUTS;  -- number of inputs used in the particular layer
    //  --pdfLut         : T_GP_PDF_LUT;     -- Definition of the look-up table
    T_GP_MEAN_DIST_PHI  meanDistPhi; //Definition of the meanDistPhi as a function of refLayer
  T_GP_SEL_DIST_PHI_SHIFT selDistPhiShift; // Definition of the distPhiMask as a function of refLayer
  ap_uint<4> selPhiShift; // integer range 0 to 10;    -- Used to find the phiMask
  T_GP_SEL_DIST_PHI_MSB_SHIFT distPhiMsbShift;
} T_GP_1LAY_PAT_CONF;

// type T_GP_ALL_LAY_PAT_CONF is array (0 to GP_MAX_LAYERS-1) of T_GP_1LAY_PAT_CONF;
typedef  T_GP_1LAY_PAT_CONF T_GP_ALL_LAY_PAT_CONF[GP_MAX_LAYERS];

//  -- definition of configuration of all pattern units
//  -- this part of the definition is common for both patterns handled by a
//  -- single pattern unit

// type T_GP_ALL_PATS_CONF is array (0 to GP_N_OF_PATS-1) of T_GP_ALL_LAY_PAT_CONF;
typedef T_GP_ALL_LAY_PAT_CONF T_GP_ALL_PATS_CONF[GP_N_OF_PATS];
typedef int T_GP_PT_CODE;

typedef struct {
  T_GP_PT_CODE iPt1;
  T_GP_PT_CODE iPt2;
  T_GP_PT_CODE iPt3;
  T_GP_PT_CODE iPt4;
  ap_int<2> iCharge;
  ap_uint<2> iEta;
  ap_uint<1> is_quad;
} T_GP_PAT_ATTRIBS;

// type T_GP_ALL_PATS_ATTRIBS is array (0 to GP_N_OF_PATS-1) of T_GP_PAT_ATTRIBS;
typedef T_GP_PAT_ATTRIBS T_GP_ALL_PATS_ATTRIBS[GP_N_OF_PATS];

typedef ap_uint<4*GP_SELECTED_DIST_PHI_BITS> T_PATTERN [18][1024];
typedef T_PATTERN T_PATTERNS[20];

// Record describing output of the gp_common_part
typedef struct {
  T_GP_REF_LAYER  ref_layer;
  T_GP_HIT_PHI  ref_phi;
  T_GP_HIT_ETA  ref_eta;
  ap_uint<1> ref_found;
  T_GP_CONAR_OUTS conar_out;
} T_GP_COMMON_PART_OUT;

// Constant describing delay on the output of GP_COMMON_PART
  const int N_GPC_DEL = 1;
  const int GP_WEIGHT_SUM_WIDTH = 12;
  typedef ap_uint<GP_WEIGHT_SUM_WIDTH> T_GP_WEIGHT_SUM;

const int GP_FIRED_LAY_CNT_WIDTH = 5;
typedef ap_uint<GP_FIRED_LAY_CNT_WIDTH> T_GP_FIRED_LAY_CNT; // is integer range 0 to GP_MAX_LAYERS;

// Signals used to delay signals for inputs of GPU sets
typedef struct {
    T_GP_WEIGHT_SUM weight;
    ap_uint<1> fired;
  } T_GPU_OUTPUT;

const T_GPU_OUTPUT gpu_output_init = {
 .weight = 0,
 .fired = 0,
};

typedef ap_uint<1> T_GP_FIRED_VEC [GP_MAX_LAYERS+1]; // WARNING! It was indexed starting from -1 to GP_MAX_LAYERS!
typedef struct {
    T_GP_WEIGHT_SUM weight_sum;
    T_GP_FIRED_LAY_CNT fired_cnt;
    //-- due to strange behavior of different simulators we must provide one more element,
    //-- even though it is never assigned...
    //-- Therefore to layer N there is bit N+1 assigned.
    T_GP_FIRED_VEC fired_vec;
  } T_GPU_RESULT;

const T_GPU_RESULT gpu_result_init = {
  .weight_sum = 0,
  .fired_cnt = 0,
  .fired_vec = {0},
};

typedef struct {
  T_GPU_RESULT res;
  ap_int<8> best;
} T_GPU_RESULT_JOINT;

const T_GPU_RESULT_JOINT gpu_result_joint_init = {
		.res = gpu_result_init,
		.best = -1,
};
typedef struct {
    ap_int<8> best_pat; // integer range 0 to 4*GP_N_OF_PATS-1;
    T_GP_FIRED_LAY_CNT fired_cnt;
    T_GP_FIRED_VEC fired_vec;
    T_GP_WEIGHT_SUM weight_sum;
    T_GP_BX_NUM bx_out;
    T_GP_HIT_PHI ref_phi;
    T_GP_HIT_ETA ref_eta;
    T_GP_HIT_PHI corr_ref_phi;
    T_GP_REF_LAYER ref_layer;
    ap_uint<1> found;
    ap_uint<1> load;
  } T_GP_OUTPUT;

const T_GP_OUTPUT GP_OUTPUT_EMPTY = {
    .best_pat     = 0,
    .fired_cnt    = 0,
    .fired_vec    = {0},
    .weight_sum   = 0,
    .bx_out       = 0,
    .ref_phi      = 0,
    .ref_eta      = 0,
    .ref_layer    = 0,
    .corr_ref_phi = 0,
    .load         = 0,
    .found        = 0,
    };

T_GP_HIT_INTERNAL inline hit2internal (
    const T_GP_HIT &hit)
{
  T_GP_HIT_INTERNAL res;
  res          = GP_EMPTY_HIT_INTERNAL;
  res.active   = hit.active;
  res.phi      = hit.phi;
  res.dist_phi = 0;
  return res;
}

//  impure function pdf_lut_init_old (
//    constant pat_num, layer_num : integer)
//    return T_PDF_ROM;


#endif //__H_GOLDEN_PROC_PKG__
