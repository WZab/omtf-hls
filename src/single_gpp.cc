#include <stdlib.h>
#include <stdio.h>
#include <ap_fixed.h>
#include "../include/omtf_constants.h"
#include "golden_proc_pkg.h"
#include "omtf_functions.h"
#include "../include/conn_area_init.h"
#include "../include/ref_hits.h"

//Function that selects the best input
int find_best(ap_int<7> ndists[6], ap_uint<1> activs[6]) {
#pragma HLS PIPELINE II=1
#pragma HLS LATENCY max=2
#pragma HLS ARRAY_PARTITION variable=ndists complete
#pragma HLS ARRAY_PARTITION variable=activs complete
	int i;
	int res = -1;
	bool first = true;
	ap_uint<7> best_dist;
	for (i = 0; i < 6; i++) {
		if (activs[i] == 1) {
			if (first) {
				best_dist = abs(ndists[i]);
				res = i;
				first = false;
			} else {
				if (abs(ndists[i]) < best_dist) {
					best_dist = abs(ndists[i]);
					res = i;
				}
			}
		}
	}
	return res;
}

T_GPU_RESULT_JOINT single_gpp(const int pn, const int mmax,
		const T_GP_CONAR_OUTS conar, const T_PATTERN pattern,
		const T_GP_ALL_LAY_PAT_CONF patconf, const int ref_hit_nr) {
// Added to allow standalone optimization
//#pragma HLS INLINE
#pragma HLS PIPELINE II=1
//#pragma HLS INTERFACE ap_none port=pn
//#pragma HLS INTERFACE ap_none port=mmax
//#pragma HLS INTERFACE ap_none port=conar
//#pragma HLS INTERFACE ap_stable port=pattern
//#pragma HLS INTERFACE ap_stable port=patconf
//#pragma HLS INTERFACE ap_none port=ref_hit_nr
//#pragma HLS INTERFACE ap_none port=gp_res_single
#pragma HLS DATA_PACK variable=patconf
#pragma HLS ARRAY_PARTITION variable=conar dim=1
#pragma HLS ARRAY_PARTITION variable=conar dim=2
#pragma HLS DATA_PACK variable=conar

//#pragma HLS INTERFACE ap_none port=return
#pragma HLS ARRAY_PARTITION variable=pattern dim=1
#pragma HLS ARRAY_PARTITION variable=CONARS_DEF complete
#pragma HLS ARRAY_PARTITION variable=patconf complete
//#pragma HLS ARRAY_PARTITION variable=best_pat complete
//The above created an error:
//ERROR: [XFORM 203-103] Cannot partition array 'best_pat' (src/single_gpp.cc:11): variable is not an array.
#pragma HLS ARRAY_PARTITION variable=REF_HIT_CONF complete
//Tu musze przekazac dane do funkcji liczacej jeden pattern
//Jakie dane wejsciowe powinna dostac?
//conar,swoj egzemplarz pats_conf,ref_hit_nr,
	T_GPU_RESULT_JOINT gp_res[4] = { [0 ... 3]=gpu_result_joint_init };
	gp_res[0].best = 4 * pn;
	gp_res[1].best = 4 * pn + 1;
	gp_res[2].best = 4 * pn + 2;
	gp_res[3].best = 4 * pn + 3;
#pragma HLS UNROLL
#pragma HLS DATA_PACK variable=gp_res
#pragma HLS ARRAY_PARTITION variable=gp_res complete
	//I zmienna przechowujaca najlepiej dopasowany podwzorzec
	//Browse consecutive layers
	//Sometimes we must remember the contribution from the previous layer
	ap_uint<7> prev_w[4] = { 0, 0, 0, 0 };
#pragma HLS ARRAY_PARTITION variable=prev_w complete
	T_GPU_RESULT_JOINT res = gpu_result_joint_init;
#pragma HLS DATA_PACK variable=res
	//Musimy koniecznie od razu dac pelen dostep do tej struktury:
//#pragma HLS_ARRAY_PARTITION variable=res.fired_vec

	//Algorytm powinien dzialac tak, ze najpierw obliczamy wklad od warstwy.
	//Jesli jest to warstwa normalna, to potem dodajemy go do wynikow dla wzorca.
	//Jesli jest to warstwa bendingowa, to wpisujemy go do "poczekalni"
	//Uwaga! Poniewaz warstwy bendingowe dzialaja z poprzednia warstwa, latwiej mi bedzie przegladac
	//warstwy od konca!
	bool was_bending = false;
	for (ap_int<6> lay_num = GP_MAX_LAYERS - 1; lay_num >= 0; lay_num--) {
//#pragma HLS DATAFLOW
		ap_int<7> adist = -1;	//The distance of the best hit
		int kbest = -1;	//The best hit
		bool first_entry = true;
		bool is_bending = CONARS_DEF[0][lay_num].bending;//We remember if the current partition is the bending one
		//weights in a layer
		ap_uint<7> ws[4] = { 0, 0, 0, 0 };
		//We store the location of the nearest hit in the
		ap_int<7> ndists[6] = { 0, 0, 0, 0, 0, 0 };
		ap_uint<1> activs[6] = { 0, 0, 0, 0, 0, 0 };
		//In the inputs belonging to the connection area we find the hit nearest to the pattern
		for (ap_uint<3> inp_num = 0; inp_num < 6; inp_num++) { //Is it the right range?
//#pragma HLS UNROLL factor=1
			//Musze przypomniec sobie jak kodowane sa PDFy i jak dziala shift we wzorcu!
			//Generalnie uzywamy distMeanPhi.
			//Check if the hit is active
			//Naszym celem jest znalezienie hitu, polozonego najblizej wzorcowego toru.
			//Sprawdzamy wiec wszystkie hity i znajdujemy ten o najmniejszej roznicy
			if (conar[lay_num][inp_num].active == 1) {
				ap_int< GP_PHI_INTERNAL_WIDTH + 1> ndist;
				bool is_ndist_ok;
				{
					ndist =
							conar[lay_num][inp_num].dist_phi
									- patconf[lay_num].meanDistPhi[REF_HIT_CONF[ref_hit_nr].ref_layer];
					is_ndist_ok = (ndist >= -64) && (ndist <= 63);
				}
				if (is_ndist_ok) {
					activs[inp_num] = 1;
					ndists[inp_num] = ndist;
				}
			}
		}
		//Mamy odleglosci dla poszczegolnych wejsc, teraz znajdujemy najblizsze
		kbest = find_best(ndists,activs);
		//printf("Patnum:%d\n",pat_num);
		//Przeglad wejsc skonczony, mamy najlepiej polozony hit.
		if (kbest > -1) {
			adist = ndists[kbest];
			ap_uint<10> lut_addr = 128 * REF_HIT_CONF[ref_hit_nr].ref_layer
					+ (adist + 64);
			ap_uint<28> wall = pattern[lay_num][lut_addr];
			for (int i = 0; i < 4; i++) {
//#pragma HLS UNROLL
				if (i < mmax) {
					ws[i] = (wall >> (i*7)) & 0x3f;
					if (0) {
						//if (ws[i]) {
						printf(
								"Pattern:%d, ref_hit_nr=%d, ref_layer=%d, layer=%d, address=%d, weight=%d\n",
								(int) pn * 4 + i, ref_hit_nr,
								REF_HIT_CONF[ref_hit_nr].ref_layer,
								(int) lay_num, (int) lut_addr, (int) ws[i]);
					}
				}
			}
		}
		//Teraz nasze dzia��ania zale���� od tego czy jest to werstwa bendingowa
		if (is_bending) {
			//Bending - tylko wpisujemy do poczekalni
			for (int i = 0; i < 4; i++)
				if (i < mmax) {
					prev_w[i] = ws[i];
				}
			was_bending = true;
		} else {
			//Warstwa normalna
			if (was_bending) {
				for (int m = 0; m < 4; m++) {
//#pragma HLS UNROLL
					if (m < mmax) {
						if ((ws[m]) && (prev_w[m])) {
							gp_res[m].res.weight_sum += ws[m] + prev_w[m];
							gp_res[m].res.fired_cnt += 2;
							gp_res[m].res.fired_vec[lay_num] = 1;
							gp_res[m].res.fired_vec[lay_num + 1] = 1;
						}
					}
				}
				was_bending = false;
			} else {
				for (int m = 0; m < 4; m++) {
//#pragma HLS UNROLL
					if (m < mmax) {
						if (ws[m]) {
							gp_res[m].res.weight_sum += ws[m];
							gp_res[m].res.fired_cnt += 1;
							gp_res[m].res.fired_vec[lay_num] = 1;
						}
					}
				}
			}
		} //is_bending?
	} //Layers
//At the end we select the best matching pattern from 2 or 4.
	if (mmax == 2) {
		res = max_gpp_res(gp_res[0], gp_res[1]);
	} else {
		T_GPU_RESULT_JOINT res1 = max_gpp_res(gp_res[0], gp_res[1]);
		T_GPU_RESULT_JOINT res2 = max_gpp_res(gp_res[2], gp_res[3]);
		res = max_gpp_res(res1, res2);
	}
	return res;
}
