#include <stdlib.h>
#include <stdio.h>
#include <ap_fixed.h>
#include "../include/omtf_constants.h"
#include "golden_proc_pkg.h"
#include "../include/ref_hits.h"
#include "../include/patterns.h"
#include "../include/conn_area_init.h"
#include "omtf_functions.h"
#include "../pout.h"

T_GP_OUTPUT golden_proc_hls(const T_GP_BX_NUM bx_num, const ap_uint<1> load, const T_GP_HITS_ARRAY hits_in) {
#pragma HLS PIPELINE II=1
#pragma HLS INTERFACE ap_none port=bx_num
#pragma HLS INTERFACE ap_none port=load
#pragma HLS INTERFACE ap_none port=hits_in
//#pragma HLS DATA_PACK variable=hits_in
#pragma HLS INTERFACE ap_ctrl_hs port=return
//#pragma HLS DATA_PACK variable=return
#pragma HLS ARRAY_PARTITION variable=patterns dim=1
#pragma HLS ARRAY_PARTITION variable=patterns dim=2
#pragma HLS ARRAY_PARTITION variable=hits_in complete dim=0
#pragma HLS ARRAY_PARTITION variable=CONARS_DEF complete
#pragma HLS ARRAY_PARTITION variable=pats_conf complete
#pragma HLS ARRAY_PARTITION variable=pats_atribs complete
#pragma HLS ARRAY_PARTITION variable=REF_HIT_CONF complete

#pragma HLS UNROLL
	ap_uint<1> ref_hit_bits[GP_N_OF_REF_HITS];
#pragma HLS ARRAY_PARTITION variable=ref_hit_bits complete
	T_GP_REF_HIT_DATA ref_hits_data;
#pragma HLS ARRAY_PARTITION variable=ref_hits_data complete
	T_GP_OUTPUT res;
#pragma HLS DATA_PACK variable=res
#pragma HLS ARRAY_PARTITION variable=res.fired_vec
	//Copy the input hits
	T_GP_HITS_ARRAY hits_in_s;
#pragma HLS ARRAY_PARTITION variable=hits_in_s complete
#pragma HLS DATA_PACK variable=hits_in_s
	//Check if we have any hits
	for (int i = 0; i < GP_MAX_LAYERS; i++)
#pragma HLS UNROLL
		for (int j = 0; j < GP_MAX_INS_IN_LAYER; j++)
#pragma HLS UNROLL
			hits_in_s[i][j]=hits_in[i][j];
			//printf("hit: layer=%d, entry=%d, phi=%d\n", i, j,hits_in_s[i][j].phi);
	//Find the reference hits
	// REF_HIT_CONF must be taken from a header file
	ref_hit_selector(REF_HIT_CONF, hits_in_s, ref_hit_bits, ref_hits_data);
	// Now we take the consecutive reference hits
	int ref_hit_nr = prior_enc(load, ref_hit_bits);
	//Find and populate the connection area
	int best_npat = -1;
	T_GPU_RESULT best_res = gpu_result_init;
	int best_pats[GP_N_OF_PATS] = {[0 ... GP_N_OF_PATS-1] = -1};
	T_GPU_RESULT gp_res[GP_N_OF_PATS] = {[0 ... GP_N_OF_PATS-1] = gpu_result_init};

	T_GP_CONAR_OUTS conar;
#pragma HLS DATA_PACK variable=conar
#pragma HLS ARRAY_PARTITION variable=conar complete
	if (ref_hit_nr < 128) {
		//printf("Ref hit nr %d from reference layer %d\n",ref_hit_nr,REF_HIT_CONF[ref_hit_nr].ref_layer);
		conn_area_builder(hits_in_s, REF_HIT_CONF[ref_hit_nr].conar_nr,
				ref_hits_data[ref_hit_nr], conar);
		//Now browse the patterns and find the best matched one
		//Now report the hits available at the output of the connection area builder
		//printf("Output of the connection area:\n");
		for (int i = 0; i < GP_MAX_LAYERS; i++)
			for (int j = 0; j < 6; j++)
				if (conar[i][j].active)
					continue;
					//printf("hit: layer=%d, entry=%d, phi=%d, dist_phi=%d\n", i, j,conar[i][j].phi,(int) conar[i][j].dist_phi);
		ap_uint<5> pat_num = 0; //, lay_num = 0, inp_num = 0;
		//Here should be the real implementation of the GP algorithm...
		//Should I reuse one_l_one_p, or reimplement it from the scratch?
		//Muszę pamiętać o niuansach związanych z warstwami bendingowymi.
		for (pat_num = 0; pat_num < GP_N_OF_PATS; pat_num++) { //N_OF_PATTERNS trzeba zmienić!?
#pragma HLS UNROLL
			//Obsługujemy maksymalnie 4 wzorce w jednej komórce pamięci.
			//conar,swój egzemplarz pats_conf,ref_hit_nr,
			int mmax = pats_atribs[pat_num].is_quad ? 4 : 2;
			T_GPU_RESULT_JOINT gppres = single_gpp(pat_num,mmax,conar,patterns[pat_num],pats_conf[pat_num],ref_hit_nr);
			best_pats[pat_num]=gppres.best;
			gp_res[pat_num]=gppres.res;
			//Wybieramy najlepszy wzorzec
			//A może wstępną selekcję mógłby zrobić już sam GPP?
		} //Patterns
		  // Here we have the best pattern from each gpp
		for (int m = 0; m < GP_N_OF_PATS; m++) {
#pragma HLS UNROLL
			if (gp_res[m].fired_cnt) {
				//The pattern is somehow matched
				if (best_npat == -1) {
					best_npat = best_pats[m];
					best_res = gp_res[m];
				} else if (is_gp_res_better(best_res, gp_res[m])) {
					best_npat = best_pats[m];
					best_res = gp_res[m];
				}
			}
		}

	} //ref_hit <= 128
	res = GP_OUTPUT_EMPTY;
	if(best_npat > -1) {
		res.best_pat = best_npat;
		res.fired_cnt = best_res.fired_cnt;
		for(int i=0;i<GP_MAX_LAYERS;i++)
			res.fired_vec[i] = best_res.fired_vec[i];
		res.weight_sum = best_res.weight_sum;
		res.bx_out = bx_num;
		res.ref_layer= REF_HIT_CONF[ref_hit_nr].ref_layer;
		res.ref_eta = ref_hits_data[ref_hit_nr].eta;
		res.ref_phi = ref_hits_data[ref_hit_nr].phi;
		res.corr_ref_phi = ref_hits_data[ref_hit_nr].phi + pats_conf[best_npat>>2][GP_BASE_LAYER].meanDistPhi[REF_HIT_CONF[ref_hit_nr].ref_layer];
		res.found = 1;
		res.load = load;
	}
	return res;
}
;

/*
 * Jak jest zszywany adres dla LUT? Informację znajduję w pliku one_l_one_p.vhd
 * Zszywamy go z numeru warstwy referencyjnej (trzy bity)
 * i z v_dist_phi (7 bitów), przy czym przeprowadzana jest tu dość dziwna operacja -
 * negujemy bit znaku, OK! To po prostu przesuwa nam zero na środek wzorca.
 */

/*
 * A jak robione jest zszywanie po 4 wzorce w jeden LUT?
 * OK. Zbiory "pdfs/pdf_..." już to robią za mnie.
 * Czy w takim razie w algorytmie tutaj powinieniem załatwić już
 * grupowanie wzorców po 4? Chyba tak, bo daje to wymierne oszczędności.
 * (coś tam liczyliśmy raz, a potem wykorzystywaliśmy dla 4 wzorców)
 */

