#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <ap_int.h>
#include <ap_utils.h>
//#include "golden_proc_pkg.h"
static ap_uint<1> sdin[128];

void prior_sub_enc(ap_uint<1> rhb[8], ap_uint<4> *code) {
#pragma HLS LATENCY max=0
#pragma HLS INLINE
#pragma HLS UNROLL
	ap_uint<1> flag;
	*code = 8;
	//printf("b:");
	flag = 1;
	for (int i = 0; i < 8; i++) {
		//printf("%d",(int)rhb[i]);
		if ( flag && (rhb[i] == 1)) {
			*code = i;
			flag = 0;
		}
	}
	//printf("\n");
}

int prior_enc(ap_uint<1> load, ap_uint<1> ref_hit_bits[128]) {
#pragma HLS PIPELINE II=1
//#pragma HLS INLINE
#pragma HLS UNROLL
//#pragma HLS LATENCY min=4
#pragma HLS ARRAY_PARTITION variable=ref_hit_bits complete
#pragma HLS ARRAY_PARTITION variable=sdin complete
	ap_uint<1> flag;
	ap_uint<4> sub_code[16];
	flag = 1;
	int code = 0;
#pragma HLS ARRAY_PARTITION variable=sub_code complete
	//Mamy 16 kanałów po 8 wejść
	code = 128;
	for (int i = 0; i < 16; i++) {
//#pragma HLS UNROLL
		if(flag) {
			prior_sub_enc(&sdin[8 * i], &sub_code[i]);
			if (sub_code[i] < 8) {
				code = i * 8 + sub_code[i];
				//printf("found: %d\n",(int) code);
				flag = 0;
			}
		}
	}
	//Loading the input data
	if (load == 1) {
		for (int i = 0; i < 128; i++) {
//#pragma HLS UNROLL
			sdin[i] = ref_hit_bits[i];
		}
	} else {
		if (code < 128)
			sdin[code] = 0;
	}
	return code;
}
