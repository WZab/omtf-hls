
#ifndef __H_OMTF_FUNCTIONS__
#define __H_OMTF_FUNCTIONS__
#include "golden_proc_pkg.h"
#include "../include/omtf_constants.h"

T_GP_OUTPUT golden_proc_hls(const T_GP_BX_NUM bx_num,const ap_uint<1> load,const T_GP_HITS_ARRAY hits_in);

void ref_hit_selector(const T_GP_REF_HIT_DEFS &ref_hit_conf,
  const T_GP_HITS_ARRAY &hits_in,
  ap_uint<1> ref_hit_bits[GP_N_OF_REF_HITS],
  T_GP_REF_HIT_DATA &ref_hits_data);

int prior_enc(ap_uint<1> load, ap_uint<1> ref_hit_bits[GP_N_OF_REF_HITS]);

void conn_area_builder (
  const T_GP_HITS_ARRAY hits_in,
  const T_GP_CONAR_NR conar_nr,
  const T_GP_HIT refHit,
  T_GP_CONAR_OUTS &conar_out);

bool inline is_gp_res_better(T_GPU_RESULT r1, T_GPU_RESULT r2) {
	if (r2.fired_cnt > r1.fired_cnt)
		return true;
	if (r2.fired_cnt < r1.fired_cnt)
		return false;

	if (r2.weight_sum > r1.weight_sum)
		return true;
	return false;
}
T_GPU_RESULT_JOINT inline max_gpp_res(const T_GPU_RESULT_JOINT r1, T_GPU_RESULT_JOINT r2) {
#pragma HLS latency min=1
	if (is_gp_res_better(r1.res,r2.res)) {
		return r2;
	} else {
		return r1;
	}
}

T_GPU_RESULT_JOINT single_gpp(const int pn, const int mmax, const T_GP_CONAR_OUTS conar, const T_PATTERN pattern, const T_GP_ALL_LAY_PAT_CONF patconf, const int ref_hit_nr);

#endif
