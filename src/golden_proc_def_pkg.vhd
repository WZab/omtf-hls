-------------------------------------------------------------------------------
-- Title      : golden pattern processor definition
-- Project    : CMS OTF
-- This file should be generated automatically!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
use work.gold_proc_pkg.all;

nction all_pats_conf_init
  return T_GP_ALL_PATS_CONF is
begin  -- function all_pats_conf_init

end function all_pats_conf_init;
constant gold_proc_conf : T_GP_ALL_PATS_CONF :=
  (
    -- Definition of all patterns
    (
      -- Pattern 0
      -- Definition of all layers in the pattern nr 0
      (
        -- Layer 0
        nOfPhis         => 4;
        selPhiShift     => 3;
        pdfLut          => (
          -- 1024 18-bit values, no comma after the last value!
          b"000111000111000111",
          b"100110100111010110"
          ),
        meanDistPhi     => (
          -- GP_MAX_REF_LAYERS integers from range 0 to 32,
          -- no comma after the last value!
          4,
          5,
          6
          ),
        selDistPhiShift => (
          -- GP_MAX_REF_LAYERS integers from range 0 to 32,
          -- no comma after the last value!
          4,
          5,
          6
          ),
        distPhiMsbShift => (
          -- GP_MAX_REF_LAYERS integers from range 0 to 32,
          -- no comma after the last value!
          4,
          5,
          6
          )
        ),
      (
      -- Next layer. No comma, after the last layer!
      )
      ),
    (
    -- Next pattern. No comma after the last pattern!
    )
    )


