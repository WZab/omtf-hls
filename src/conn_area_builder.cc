/*-------------------------------------------------------------------------------
 -- Title      : Connection area builder
 -- Project    : CMS OTF
 -------------------------------------------------------------------------------
 -- File       : conn_area_builder.vhd
 -- Author     : WZab
 -- Company    : ISE PW, IFD UW
 -- Created    : 2014-02-11
 -- Last update: 2017-11-22
 -- Platform   :
 -- Standard   : VHDL'93
 -------------------------------------------------------------------------------
 -- Description: "Connection Area" builder
 --              Currently introduces latency of 1 clkp
 -------------------------------------------------------------------------------
 -- Copyright (c) 2014
 -------------------------------------------------------------------------------
 -- Revisions  :
 -- Date        Version  Author  Description
 -- 2014-02-11  1.0      WZab    Created
 -------------------------------------------------------------------------------
 */
#include "golden_proc_pkg.h"
#include "../include/conn_area_init.h"
// CONARS_DEF powinno pochodzić z pliku nagłówkowego!

void conn_area_builder(const T_GP_HITS_ARRAY hits_in,
		const T_GP_CONAR_NR conar_nr, const T_GP_HIT refHit,
		T_GP_CONAR_OUTS &conar_out) {
	//#pragma HLS_PIPELINE
	//#pragma HLS INTERFACE ap_none port=hits_in
	//#pragma HLS DATA_PACK variable=hits_in
	//#pragma HLS INTERFACE ap_none port=conar_nr
	//#pragma HLS DATA_PACK variable=conar_nr
	//#pragma HLS INTERFACE ap_none port=refHit
	//#pragma HLS DATA_PACK variable=refHit
	//#pragma HLS INTERFACE ap_none port=conar_out
	//#pragma HLS DATA_PACK variable=conar_out
#pragma HLS ARRAY_PARTITION variable hits_in complete
#pragma HLS ARRAY_PARTITION variable conar_out complete
#pragma HLS UNROLL
//#pragma HLS INLINE
	int i, j;
	int layer;
	int nin;
	for (i = 0; i < GP_MAX_LAYERS; i++)
#pragma HLS UNROLL
		for (j = 0; j < 6; j++)
			conar_out[i][j] = GP_EMPTY_HIT_INTERNAL;
	for (layer = 0; layer < GP_MAX_LAYERS; layer++) {
#pragma HLS UNROLL
		int efirst = CONARS_DEF[conar_nr][layer].first;
		// Iterate through inputs
		for (nin = 0; nin < GP_MAX_INS_IN_LAYER; nin++) {
#pragma HLS UNROLL
			if ((nin < CONARS_DEF[conar_nr][layer].len) && (nin < 6)) {
				conar_out[layer][nin] = hit2internal(
						hits_in[layer][nin + efirst]);
				// Calculate the dist_phi depending on the type of layer
				if (CONARS_DEF[conar_nr][layer].bending)
					conar_out[layer][nin].dist_phi =
							hits_in[layer][nin + efirst].phi;
				else
					conar_out[layer][nin].dist_phi =
							hits_in[layer][nin + efirst].phi - refHit.phi;
			}
		};  // nin
	};  // layer
	return;
}

