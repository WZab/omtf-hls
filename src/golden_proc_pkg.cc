#include "golden_proc_pkg.h"

/*
  impure function pdf_lut_init_old (
    constant pat_num, layer_num : integer)
    return T_PDF_ROM is
    file coefs       : text;
    variable line_in : line;
    variable val     : std_logic_vector(35 downto 0);
    --variable fname   : string;
    variable res     : T_PDF_ROM := (others => (others => '0'));
  begin  -- function pdf_lut_init
    file_open(coefs, "pdfs/pdf_" & integer'image(pat_num) & "_" & integer'image(layer_num), read_mode);
    assert (1 = 0) report "mem[" & integer'image(pat_num) & "," & integer'image(layer_num) & "] " severity note;
    for i in 0 to 1023 loop
      readline(coefs, line_in);
      read(line_in, val);
      res(i) := val;
    end loop;  -- i
    file_close(coefs);
    assert (1 = 0) report "end of pdf generation " severity note;
    return res;
  end function pdf_lut_init_old;

end package body golden_proc_pkg;
*/

