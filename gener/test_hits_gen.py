#!/usr/bin/python
# -*- coding: utf-8 -*-
ProcessorNum=0
import xml.etree.ElementTree as et
from omtf_constants import *

plik1=et.ElementTree(file="./TestEvents.xml")
#Pobieramy główny element
er=plik1.getroot()
##To powinien być element typu OMTF_Events
#if er.tag != "OMTF_Events":
#To powinien być element typu OMTF
if er.tag != "OMTF":
    raise Exception("Wrong type of the root element!")
#Otwieramy plik wynikowy
fo=open("../conf/hits.ini","w")
for ev in er.getchildren():
    if ev.tag != "Event":
        raise Exception("Wrong type of the child element!")
    for bx in ev.findall("bx"):
        nbx = int(bx.attrib["iBx"])
        #Odczytujemy kolejne procesory
        for prc in bx.findall("Processor"):
          if int(prc.attrib["iProcessor"])==ProcessorNum:
            #Odczytujemy kolejne warstwy
            for lay in prc.findall("Layer"):
              lay_nr = int(lay.attrib["iLayer"])
              #Odczytaj hity w obrębie warstwy
              for hit in lay.findall("Hit"):
                  entry = int(hit.attrib["iInput"])
                  #entry = 3 # To test operation of the sorter
                  phi = int(hit.attrib["iPhi"])
                  #korygujemy phi do przedziału -1024,1023
                  while phi<-1024:
                     phi += 2048
                  while phi > 1023:
                     phi -= 2048
                  #Zapisz każdy hit do zbioru wynikowego
                  line = str(lay_nr)+" "+str(entry)+" "+str(phi)+" "+str(nbx)+"\n"
                  fo.write(line)
fo.close()
# Plik zdefinicją hitów dla syntezy
def bstr(num):
    if num<0:
        num += 1<<32
    return '{0:032b}'.format(num)

fo=open("../conf/hits_slv.ini","w")
for ev in er.getchildren():
    if ev.tag != "Event":
        raise Exception("Wrong type of the child element!")
    for bx in ev.findall("bx"):
        nbx = int(bx.attrib["iBx"])
        #Odczytujemy kolejne warstwy
        for lay in bx.findall("Layer"):
            lay_nr = int(lay.attrib["iLayer"])
            #Odczytaj hity w obrębie warstwy
            for hit in lay.findall("Hit"):
                entry = int(hit.attrib["iInput"])
                #entry = 3 # To test operation of the sorter
                phi = int(hit.attrib["iPhi"])
                #korygujemy phi do przedziału 0,1023
                #phi = (phi % 1024)
                #Zapisz każdy hit do zbioru wynikowego
                line = bstr(lay_nr)+"\n"+bstr(entry)+"\n"+bstr(phi)+"\n"+bstr(nbx)+"\n"
                fo.write(line)
fo.close()

