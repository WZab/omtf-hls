#!/usr/bin/python
# -*- coding: utf-8 -*-
import xml.etree.ElementTree as et
from omtf_constants import *
from pat_def import *
from pat_wr import *
import random as r
from operator import itemgetter, attrgetter
pat_skip=1 # to reduce number of patterns!
#We prepare variables for storing the minimum and maximum value of PDF
pdfmax=-1000000
pdfmin=1000000
pat_nr=0
ptcodes=set()
#Create the main list, which will store patterns definitions
#Now we can iterate over the children, which should be the "GP" elements
# When the pattern definition file will be correct, we will iterate over all pattern definitions:
# for gp in er.getchildren():
# But now we have only one such definition:
def read_pats(fname):
    global pdfmin,  pdfmax,  pat_nr,  ptcodes
    file1=et.ElementTree(file=fname)
    er=file1.getroot()
    #Get the root element
    #This should be an "OMTF" element
    if er.tag != "OMTF":
        raise Exception("Wrong type of the root element!")
    gpp_def = []
    for gp in er.getchildren():
        if gp.tag != "GP":
            raise Exception("Wrong type of the child element!")
        pat = otf_pattern(2, pat_nr) # To trzeba by zmienić, bo przecież numery wzorców ostatecznie ustalimy po selekcji...
        for an in ['iCharge', 'iEta', 'iPhi',  'iPt1', 'iPt2', 'iPt3', 'iPt4' ]:
            setattr(pat, an, int(gp.attrib[an]))
        #Create list of ptcodes
        ptcodes.add(pat.iCharge*pat.iPhi)
        #Now read the layers
        for lay_def in gp.getchildren():
            if lay_def.tag != "Layer":
                raise Exception("Wrong type of the child element! "+lay.tag)
            lay_nr = int(lay_def.attrib["iLayer"])
            lay = otf_layer(lay_nr)
            # Now we should analyze layer's attributes
            mdp_data=[]
            dp_data=[]
            dmp_data=[]
            for d in lay_def.findall("RefLayer"):
                # MeanDistPhi
                v=int(d.attrib['meanDistPhi'])
                mdp_data.append(v)
                # DistPhiShift
                v=int(d.attrib['selDistPhiShift'])
                dp_data.append(v)
                v=int(d.attrib['distMsbPhiShift'])
                dmp_data.append(v)
            lay.add_object(mean_dist_phi(mdp_data))
            lay.add_object(dist_phi_shift(dp_data))
            lay.add_object(dist_phi_msb_shift(dmp_data))
            # PDF definition
            lay.pdf=[]
            for pdf in lay_def.findall("PDF"):
                v1=int(pdf.attrib['value1']) & ((1<<7)-1)
                v2=int(pdf.attrib['value2']) & ((1<<7)-1)
                #Now we assume 7-bit encodings
                lay.pdf.append(v1 | (v2 << 7))
                #lay.pdf.append(r.randint(0,1023))
                #print(v)
                if v1>pdfmax:
                    pdfmax=v1
                if v1<pdfmin:
                    pdfmin=v1
                if v2>pdfmax:
                    pdfmax=v2
                if v2<pdfmin:
                    pdfmin=v2
            # Layer is built, so now we can add it to the pattern
            pat.add_layer(lay)
        #Pattern is built, so we add it to the list of paterns
        gpp_def.append(pat)
        pat_nr += 1
    return gpp_def
# The first stage, read the pattern definition file, and create pattern definitions
fp1=[]
for plik in [
  "GPs_4x.xml",
  #"Patterns_ipt6_18.xml"
  ]:
    fp1+=read_pats(plik)
print "Minimal PDF value:"+str(pdfmin)
print "Maximal PDF value:"+str(pdfmax)
def my_sort(p):
   return -p.iCharge + 4*max(p.iPt1,p.iPt2,p.iPt3, p.iPt4)
#fp1.sort(key=my_sort,  reverse=True)
#Now perform selection
#fp1=[fp1[i] for i in range(0, len(fp1), pat_skip)]
gpp_def = fp1
gpp_def.sort(key=my_sort,  reverse=True)
#Renumber patterns!
for i in range(0, len(gpp_def)):
    gpp_def[i].renumber(i)
for pd in gpp_def:
    print pd.iPt1, pd.iPt2, pd.iPt3, pd.iPt4

