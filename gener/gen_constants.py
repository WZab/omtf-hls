#!/usr/bin/python
from omtf_constants import *
header1="""
/*--------------------------------------------------------
-- Basic constants for OMTF processor configuration
-- file generated automatically by Python configuration
-- processor. Please don't edit by hand
--------------------------------------------------------*/
#ifndef H_OMTF_CONSTANTS
#define H_OMTF_CONSTANTS
"""
const_defs = (('GP_N_OF_REF_HITS',n_of_ref_hits),
 ('GP_N_OF_PATS',n_of_pats),
 ('GP_MAX_LAYERS',n_of_lays),
 ('GP_MAX_REF_LAYERS',n_of_ref_lays),
 ('GP_N_OF_CONARS',n_of_conars),
 ('GP_MAX_INS_IN_LAYER',n_of_layer_ins),
 ('GP_MAX_CONAR_OUTS',n_of_conar_outs))

fo=open('../include/omtf_constants.h','w')
fo.write(header1)
for df in const_defs:
    line="#define "+df[0]+" ( "+str(df[1])+" )\n"
    fo.write(line)
fo.write("#endif\n")
fo.close()

