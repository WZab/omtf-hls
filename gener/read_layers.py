#!/usr/bin/python
# -*- coding: utf-8 -*-
import xml.etree.ElementTree as et
from omtf_constants import *
class layer_info:
    def __init__(self):
        self. hw2log={}
        self.log2hw={}
        self.ref2log={}
        self.log2ref={}
        self.log_bending=set()
        self.hw_bending=set()
        self.conars={}
        self.layer_version='00000000'
def read_layers():
    r=layer_info()
    plik1=et.ElementTree(file="./hwToLogicLayer.xml")
    #Pobieramy główny element
    er=plik1.getroot()
    #To powinien być element typu OMTF
    if er.tag != "OMTF":
        raise Exception("Wrong type of the root element!")
    #Odczytujemy numer wersji z głównego elementu
    if 'version' in er.attrib:
	r.layer_version='%8.8x' % int(er.attrib['version'],0)       
    #Teraz wczytujemy tablicę translacji  sprzętowych identyfikatorów warstw
    for el in er.findall("LayerMap"):
        hw_id = int(el.attrib['hwNumber'])
        log_id = int(el.attrib['logicNumber'])        
        bending = int(el.attrib['bendingLayer'])
        r.hw2log[hw_id]=log_id
        r.log2hw[log_id]=hw_id
        if bending==1:
            r.log_bending.add(log_id)
            r.hw_bending.add(hw_id)
    #Check if the number of layers is correct
    if len(r.hw2log) != n_of_lays:
        raise Exception("Incorrect number of layers! Found "+str(len(hw2log))+", expected "+str(n_of_lays))
    #Teraz wczytujemy tablicę translacji warstw referencyjnych
    for el in er.findall("RefLayerMap"):
        ref_id = int(el.attrib['refLayer'])
        log_id = int(el.attrib['logicNumber'])
        r.ref2log[ref_id]=log_id
        r.log2ref[log_id]=ref_id
    #Check if the number of reference layers is correct
    if len(r.ref2log) != n_of_ref_lays:
        raise Exception("Incorrect number of reference layers! Found "+str(len(ref2log))+", expected "+str(n_of_ref_lays))
    #Now we process the  information about reference hits and connection areas for the particular processor
    for pd in er.findall("Processor"):
        if int(pd.attrib['iProcessor'])==0: # Now we handle only processor 0
            #First we process connection areas
            for ic in pd.findall('LogicRegion'):
                nr_conar=int(ic.attrib['iRegion'])
                lay_defs={}
                for ld in ic.findall('Layer'):
                    nr_lay=int(ld.attrib['iLayer'])
                    nr_first=int(ld.attrib['iFirstInput'])
                    nr_entries=int(ld.attrib['nInputs'])
                    # We extract information about bending
                    if nr_lay in r.log_bending:
                        nr_bending = 1
                    else:
                        nr_bending = 0
                    lay_defs[nr_lay] = (nr_first,  nr_entries,  nr_bending)
                r.conars[nr_conar]=lay_defs
            #Then we process reference hits
            ref_hits={}
            for rh in pd.findall('RefHit'):
                rd={}
                rh_nr = int(rh.attrib['iRefHit'])
                rd['conar_nr']=int(rh.attrib['iRegion'])
                rd['entry']=int(rh.attrib['iInput'])
                rd['phi_max']=int(rh.attrib['iPhiMax'])
                rd['phi_min']=int(rh.attrib['iPhiMin'])
                rl=int(rh.attrib['iRefLayer'])
                rd['ref_layer']=rl
                rd['layer']=r.ref2log[rl]
                ref_hits[rh_nr]=rd
            r.ref_hits=ref_hits    
    return  r
