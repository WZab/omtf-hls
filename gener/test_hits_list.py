#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
ProcessorNum=int(sys.argv[1])
import xml.etree.ElementTree as et
from omtf_constants import *

plik1=et.ElementTree(file="./TestEvents.xml")
#Pobieramy główny element
er=plik1.getroot()
#To powinien być element typu OMTF_Events
if er.tag != "OMTF_Events":
    raise Exception("Wrong type of the root element!")
#Otwieramy plik wynikowy
#fo=open("../conf/hits.ini","w")
for ev in er.getchildren():
    if ev.tag != "Event":
        raise Exception("Wrong type of the child element!")
    for bx in ev.findall("bx"):
        nbx = int(bx.attrib["iBx"])
        #Odczytujemy kolejne procesory
        for prc in bx.findall("Processor"):
          if int(prc.attrib["iProcessor"])==ProcessorNum:
            #Odczytujemy kolejnych kandydatów
            for cand in prc.findall("Candidate"):
              chrg = int(cand.attrib["charge"])
              disc = int(cand.attrib["disc"])
              refl = int(cand.attrib["iRefLayer"])
              regn = int(cand.attrib["iRegion"])
              nhits = int(cand.attrib["nHits"])
              pt = int(cand.attrib["ptCode"])
              line = "BX="+str(nbx)+" chg="+str(chrg)+" weight="+str(disc)+" fired="+str(nhits)+" rgn="+str(regn)
              print(line)

