#!/usr/bin/python
import sys
#sys.exit(1)
from omtf_constants import *
from read_layers import *
ld=read_layers()
header1="""
/*
  Definition of connection areas for OMTF
*/
#ifndef H_OMTF_CONARS
#define H_OMTF_CONARS
const T_GP_ALL_CONARS_DEF CONARS_DEF = {
"""

header2="""
/*
  Version of layers definition for OMTF
*/
#ifndef H_OMTF_LAYERS_VER
#define H_OMTF_LAYERS_VER
"""
#Now let's prepare the configuration for connection areas builder
fout=open("../include/conn_area_init.h","w")
fout.write(header1)
for i in range(0,n_of_conars): #n of connection areas
    ca=ld.conars[i]
    line = "{"
    for j in range(0,n_of_lays): # n of layers
        first=ca[j][0] #was: r.randint(0,n_of_conars-1)
        line += "{"+str(first)
        length = ca[j][1] #was: r.randint(1,min(6,n_of_layer_ins-1-first))
        line += ","+str(length)
        if ca[j][2]:
           bending = "true"
        else:
           bending = "false"
        line += ","+bending+"}"
        if j!=(n_of_lays-1):
            line +=","
    if i != (n_of_conars-1):
        fout.write(line+"},\n")
    else:
        fout.write(line+"}\n")
fout.write("};\n#endif\n")
fout.close()
# Now prepare the package with information about the version of layers configuration
fout=open("../include/omtf_layers_version_pkg.h","w")
fout.write(header2)
fout.write("constant int OMTF_layers_version = 0x"+ld.layer_version+";\n")
fout.write("#endif\n")
fout.close()

