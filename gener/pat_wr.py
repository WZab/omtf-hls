#!/usr/bin/python
# -*- coding: utf-8 -*-
#Now we build the configuration
import numpy
indent=0
header1="""\
/* Pattern definition
*/
"""

def report_pattern(nr, charge, pt):
    if pt != 0:
        return "%2.2d: iCharge=%1.1d iPt=%2.2d" % (nr , charge, pt) 
    else:
        return "%2.2d: Pattern not used" % (nr,)

def write_pout(pats):
    #Print the configuration
    fout=open("../pout.h", "w")
    fout.write(header1)
    fout.write("const T_GP_ALL_PATS_CONF pats_conf={")
    for i in range(0, len(pats)):
        fout.write(pats[i].write(2))
        if i != len(pats)-1:
            fout.write(",\n")
        else:
            fout.write("\n")
    fout.write("};")
    fout.write("""
    T_GP_ALL_PATS_ATTRIBS pats_atribs={
    """)
    for i in range(0, len(pats)):
        fout.write(pats[i].write_attribs(2))
        if i != len(pats)-1:
            fout.write(",\n")
        else:
            fout.write("\n")
    fout.write("};\n")
    #fout.write("#pragma HLS ARRAY_PARTITION variable=pats_atribs complete\n")
    #fout.write("#pragma HLS ARRAY_PARTITION variable=pats_conf complete\n")
    fout.close()
    #Write list of patterns
    fout=open("../patterns.txt", "w")
    i=0
    for i in range(0,len(pats)):
        fout.write(report_pattern(4*i+0 , pats[i].iCharge, pats[i].iPt1)+"\n") 
        fout.write(report_pattern(4*i+1 , pats[i].iCharge, pats[i].iPt2)+"\n") 
        fout.write(report_pattern(4*i+2 , pats[i].iCharge, pats[i].iPt3)+"\n") 
        fout.write(report_pattern(4*i+3 , pats[i].iCharge, pats[i].iPt4)+"\n") 
    fout.close()

def write_pdfs1(pats):
    for pat in pats:
        for lay in pat.layers:
            nr_pat = lay.pattern
            nr_lay = lay.layer
            fo=open("../pdfs/pdf_"+str(nr_pat)+"_"+str(nr_lay),"w")
            for i in range(0,1024):
                fo.write(str(lay.pdf[i])+"\n")
            fo.close()

#Next function writes pdfs to the  function
header1b="""
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
use work.golden_proc_pkg.all;

package pat_lut_pkg is

function pdf_lut_init (
    constant pat_num, layer_num : integer)
    return T_PDF_ROM;

end package pat_lut_pkg;

package body pat_lut_pkg is

  function pdf_lut_init (
    constant pat_num, layer_num : integer)
    return T_PDF_ROM is
    variable val : T_PDF_ROM_AS_INT;
    variable res : T_PDF_ROM;
    variable dummy : boolean := false;
   begin    -- function pdf_lut_init
       if dummy then
           null; -- This condition is never met, but we want all real tests to be done with "elsif"
"""
def write_pdfs2(pats):
    #Print the configuration
    fout=open("../pat_lut_pkg.vhd", "w")
    fout.write(header1b)
    for pat in pats:
        for lay in pat.layers:
            nr_pat = lay.pattern
            nr_lay = lay.layer
            fout.write("elsif (pat_num="+str(nr_pat)+") and ( layer_num="+str(nr_lay) +") then\n")
            fout.write("  val:=(\n")
            for i in range(0,1024):
                fout.write("    "+str(lay.pdf[i]))
                if i == 1023:
                    fout.write("\n")
                else:
                    fout.write(",\n")
            if True:
                faux = open("./lays.txt","a+")
                for i in range(0,1024):
                    linia1="P:"+str(lay.pattern)+" L:"+str(lay.layer)+" N:"+str(i)+" V:"+str(lay.pdf[i])+"\n"
                    faux.write(linia1)
                faux.close()
            fout.write(");\n")
    fout.write("""
       end if;
       for i in 0 to 1023 loop
          res(i) := std_logic_vector(to_unsigned(val(i),18));
       end loop;
       return res;
    end function pdf_lut_init;
    """)
    fout.write("\n end package body pat_lut_pkg;\n")
    fout.close()

def write_pdfs3(pats):
    for pat in pats:
        for lay in pat.layers:
            nr_pat = lay.pattern
            nr_lay = lay.layer
            fo=open("../pdfs/pdf_"+str(nr_pat)+"_"+str(nr_lay),"w")
            for i in range(0,1024):
                fo.write('{0:036b}'.format(lay.pdf[i])+"\n")
            fo.close()
"""
 Function for writing of patterns definition in HLS-friendly form.
 Nie wiem, czy wszystkie wzorce są podefiniowane. Jak w takim razie mogę sobie z tym
 poradzić?
 Pewnie będę musiał zdefiniować pustą tablicę i zapełniać ja wzorcami!
 Czy jakoś chcemy
"""
header_pats="""
"""
def write_pdfs(pats):
    #Find the number of patterns
    npats=len(pats)
    #Find the number of layers
    nlays=-1
    for pat in pats:
      if len(pat.layers)>nlays:
        nlays = len(pat.layers)
    print str(npats)+" patterns, with "+str(nlays)+" layers"
    #Let's fill the patterns
    patterns=numpy.zeros([npats,nlays,1024],dtype=numpy.uint32)
    for pat in pats:
        for lay in pat.layers:
            nr_pat = lay.pattern
            nr_lay = lay.layer
            patterns[nr_pat,nr_lay,:]=lay.pdf
    #Now we can write the patterns    
    fout=open("../include/patterns.h", "w")
    fout.write(header_pats)
    dfp="const ap_uint<4*GP_SELECTED_DIST_PHI_BITS> patterns["+\
      str(npats)+"]["+str(nlays)+"][1024]={\n"
    #dfp="int patterns["+\
    #  str(npats)+"]["+str(nlays)+"][1024]={\n"
    fout.write(dfp)
    for np in range(0,npats):
        dfp="{\n"
        for nl in range(0,nlays):
            dfp+="  {"
            for i in range(0,1024):
                dfp+=str(patterns[np,nl,i])+","
            dfp+="},\n"
        dfp+="},\n"
        fout.write(dfp)
    fout.write("};\n")
    #fout.write("#pragma HLS ARRAY_PARTITION variable=patterns complete\n")
    fout.close()

