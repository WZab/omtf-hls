#!/usr/bin/python
# -*- coding: utf-8 -*-
import xml.etree.ElementTree as et
from omtf_constants import *
from read_layers import *
#Definicje translacji warstw są wczytane, teraz przetwarzamy definicje
#hitów referencyjnych
r=read_layers()
#Check if the number of ref_hits is correct
if len(r.ref_hits) != n_of_ref_hits:
    raise Exception("Incorrect number of reference hits! Found "+str(len(r.ref_hits))+", expected "+str(n_of_ref_hits))
#Teraz możemy zapisać już definicję hitów referencyjnych
header1="""
#ifndef __H_GP_REF_HIT_DEFS__
#define __H_GP_REF_HIT_DEFS__
const T_GP_REF_HIT_DEFS REF_HIT_CONF = {
"""
with open("../include/ref_hits.h", "w") as rhd:
    rhd.write(header1)
    for i in range(0,n_of_ref_hits): #n of connection areas
        rh=r.ref_hits[i]
        line = "{"+\
             ".layer="+str(rh['layer'])+","+ \
             ".entry="+str(rh['entry'])+","+ \
             ".ref_layer="+str(rh['ref_layer'])+","+ \
             ".conar_nr="+str(rh['conar_nr'])+","+ \
             ".min_phi="+str(rh['phi_min'])+","+ \
             ".max_phi="+str(rh['phi_max'])
        if i != (n_of_ref_hits-1):
            rhd.write(line+"},\n")
        else:
            rhd.write(line+"}\n")
    rhd.write("};\n#endif")
