#!/usr/bin/python
from omtf_constants import *
class otf_array(object):
    def __init__(self):
        pass
    def write(self,  indent):
        res=indent*" "+"."+self.name+" = {\n"
        for d in range(0, len(self.data)):
            res+= (indent+2)*" "+str(self.data[d])
            if d != len(self.data)-1:
                res+=",\n"
            else:
                res+="\n"
        res += indent*" "+"}"
        return res

class otf_uns_array(object):
    def __init__(self,indent):
        pass
    def write(self, indent):
        res=indent*" "+"."+self.name+" = {\n"
        for d in range(0, len(self.data)):
            res += str(self.data[d])+" "
            if d != len(self.data)-1:
                res+=",\n"
            else:
                res+="\n"
        res += indent*" "+"}"
        return res

class otf_uns_array_old(object):
    def __init__(self,indent):
        pass
    def write(self):
        res=indent*" "+self.name+" => (\n"
        for d in range(0, len(self.data)):
            bdata=bin(self.data[d])[2:]
            bdata='b"'+(18-len(bdata))*"0"+bdata+'"'
            res+= (indent+2)*" "+bdata
            if d != len(self.data)-1:
                res+=",\n"
            else:
                res+="\n"
        res += indent*" "+")"
        return res



class pdf_conf(otf_uns_array):
    def __init__(self,  indent,  data):
        otf_uns_array.__init__(self, indent)
        self.data = data
        self.name = "pdfLut"
        pass

class mean_dist_phi(otf_array):
    def __init__(self,  data):
        otf_array.__init__(self)
        self.data = data
        self.name = "meanDistPhi"
        pass

class dist_phi_shift(otf_array):
    def __init__(self,  data):
        otf_array.__init__(self)
        self.data = data
        self.name = "selDistPhiShift"
        pass

class dist_phi_msb_shift(otf_array):
    def __init__(self,  data):
        otf_array.__init__(self)
        self.data = data
        self.name = "distPhiMsbShift"
        pass

class otf_layer(object):
    def __init__(self,  lay_nr):
        self.layer = lay_nr
        self.objs=[]
        #self.objs.append(pdf_conf(indent+2))
    def add_object(self, obj):
        self.objs.append(obj)
    def write(self,  indent):
        res=indent * " "+"{ // Pattern:"+str(self.pattern)+" Layer:"+str(self.layer)+"\n"
        res+=indent * " "+" .nOfPhis = "+str(n_of_conar_outs)+",\n"
        res+=indent * " "+" .selPhiShift = "+str(0)+",\n"
        for o in range(0, len(self.objs)):
            res += self.objs[o].write(indent+2)
            if o != len(self.objs)-1:
                res+=",\n"
            else:
                res+="\n"
        res += "}"
        return res

class otf_pattern(object):
    def __init__(self, indent, pattern):
        self.pattern = pattern
        self.layers = []
    def add_layer(self, lay):
        lay.pattern = self.pattern
        self.layers.append(lay)
    def renumber(self,num):
        #Function used to renumber patterns after selection...
        self.pattern = num
        for lay in self.layers:
            lay.pattern = num
    def write(self, indent):
        res=indent * " "+"{ //Pattern:"+str(self.pattern)+" iCharge="+str(self.iCharge)+" iPt1="+str(self.iPt1)+" iPt2="+str(self.iPt2)+"\n"
        for o in range(0, len(self.layers)):
            res += self.layers[o].write(indent+2)
            if o != len(self.layers)-1:
                res+=",\n"
            else:
                res+="\n"
        res += "}"
        return res
    def write_attribs(self, indent):
        res =indent * " "+"{\n"
        res+=indent * " "+" .iPt1 = "+str(self.iPt1)+",\n"
        res+=indent * " "+" .iPt2 = "+str(self.iPt2)+",\n"
        res+=indent * " "+" .iPt3 = "+str(self.iPt3)+",\n"
        res+=indent * " "+" .iPt4 = "+str(self.iPt4)+",\n"
        res+=indent * " "+" .iEta = "+str(self.iEta)+",\n"
        res+=indent * " "+" .iCharge = "+str(self.iCharge)+",\n"
	if (self.iPt3 != 0) or (self.iPt4 != 0):
          res+=indent * " "+" .is_quad = true\n"
        else:
          res+=indent * " "+" .is_quad = false\n"
        res+=indent * " "+"}"
        return res

